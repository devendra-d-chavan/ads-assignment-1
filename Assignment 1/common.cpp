#include <windows.h>
#include <fstream>
#include <stdio.h>
#include <time.h>
#include <iomanip>

#include "common.h"

using std::ifstream;
using std::ofstream;
using std::ios;
using std::setw;
using std::cout;
using std::endl;

void PrintHits(const int* seeks, const int* hits, const int seeks_length, const string output_filepath)
{
	ofstream fp;
	char buff[100];

	// Initialize the buffer
	for(int i = 0; i < 100; i++)
	{
		buff[i] = '\0';
	}

	for(int i = 0; i < seeks_length; i++)
	{	
		cout << setw(6) << seeks[i] << ": " << hits[i] << endl;
	}
}

int LoadArrayFromFile(const string filepath, int** numbers, int* length)
{
	ifstream  fp;       // Input file stream

	fp.open( filepath, ios::binary | ios::ate );

	*length = fp.tellg() / sizeof(int);	
	// Allocate memory for an array
	*numbers = new int[*length];

	fp.seekg(ios::beg);
	if(!fp.is_open())
	{
		return -1;
	}

	int i = 0;
	fp.read( (char *) *numbers, (int) sizeof( int ) );
	i++;

	while( !fp.eof() ) {		
		fp.read( (char *) (*numbers + i), (int) sizeof( int ) );
		i++;
	}

	fp.close();
	return 0;
}

int* GetLookupHits_Sequential(const int* keys, const int keys_length, const int* seeks, const int seeks_length)
{
	int* hits = new int[seeks_length];
	for(int i = 0; i < seeks_length; i++)
	{
		hits[i] = false;
		for(int j = 0; j < keys_length; j++)
		{
			if(seeks[i] == keys[j])
			{
				hits[i] = true;
				break;
			}
		}
	}

	return hits;
}

int* GetLookupHits_Binary(const int* keys, const int keys_length, const int* seeks, const int seeks_length)
{
	int* hits = new int[seeks_length];
	for(int i = 0; i < seeks_length; i++)
	{
		BinarySearch(keys, seeks[i], keys_length);
		hits[i] = false;
		for(int j = 0; j < keys_length; j++)
		{
			if(seeks[i] == keys[j])
			{
				hits[i] = true;
				break;
			}
		}
	}

	return hits;
}

int BinarySearch(const int* keys, const int key, const int length)
{
	int min = 0;
	int max = length - 1;
	while (max >= min)
	{
		int mid = (min + max)/2;

		if (keys[mid] < key)
			min = mid + 1;
		else if (keys[mid] > key)
			max = mid - 1;
		else
			return mid;
	}

	return -1;
}

int BinarySearch(const int key, const int length, ifstream* fp)
{
	int min = 0;
	int max = length - 1;
	while (max >= min)
	{
		int mid = (min + max)/2;
		(*fp).seekg(mid * sizeof(int), ios::beg);

		int val;
		(*fp).read( (char *) &val, (int) sizeof( val ) );

		if (val < key)
			min = mid + 1;
		else if (val > key)
			max = mid - 1;
		else
			return mid;
	}

	// TODO: Binary search can return true/false as the mid value is not being used
	return -1;
}

int* GetLookupHits_Sequential(const string keys_filepath, const int* seeks, const int seeks_length)
{
	ifstream fp;
	fp.open(keys_filepath, ios::binary | ios::in);
	if(!fp.is_open())
	{
		return NULL;
	}

	int* hits = new int[seeks_length];
	for(int i = 0; i < seeks_length; i++)
	{
		hits[i] = false;
		int val;
		fp.clear();
		fp.seekg(ios::beg);
		fp.read( (char *) &val, (int) sizeof( val ) );
		while( !fp.eof() ) {	
			if(val == seeks[i])
			{
				hits[i] = true;
				break;
			}

			fp.read( (char *) &val, (int) sizeof( val ) );
			
		}
	}

	fp.close();

	return hits;
}

int* GetLookupHits_Binary(const string keys_filepath, const int* seeks, const int seeks_length)
{
	ifstream fp;
	fp.open(keys_filepath, ios::binary | ios::in | ios::ate);
	if(!fp.is_open())
	{
		return NULL;
	}

	int length = fp.tellg() / sizeof(int);
	fp.seekg(ios::beg);

	int* hits = new int[seeks_length];
	for(int i = 0; i < seeks_length; i++)
	{
		fp.seekg(ios::beg);
		// TODO: Binary search can return true/false as the mid value is not being used
		hits[i] = (BinarySearch(seeks[i], length, &fp) != -1);
	}

	return hits;
}