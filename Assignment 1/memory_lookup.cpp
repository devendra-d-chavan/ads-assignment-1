#include <windows.h>
#include <fstream>
#include <stdio.h>
#include <time.h>

#include "common.h"
#include "memory_lookup.h"

using std::ifstream;
using std::ofstream;
using std::ios;
using std::cout;
using std::endl;

void SequentialLookupMemory()
{
	SYSTEMTIME  beg;      // Start time
	SYSTEMTIME  end;      // End time

	const string keys_filepath = "key.pc.db";
	const string seeks_filepath = "seek.pc.db";

	int keys_length = 0, seeks_length;
	int* keys = NULL;
	int* seeks = NULL;
	LoadArrayFromFile(seeks_filepath, &seeks, &seeks_length);

	GetLocalTime( &beg );

	printf( "%02d:%02d:%02d:%06d\n", beg.wHour,
		beg.wMinute, beg.wSecond, beg.wMilliseconds * 1000 );

	LoadArrayFromFile(keys_filepath, &keys, &keys_length);

	int* hits = GetLookupHits_Sequential(keys, keys_length, seeks, seeks_length);
	GetLocalTime( &end );
	printf( "%02d:%02d:%02d:%06d\n", end.wHour,
		end.wMinute, end.wSecond, end.wMilliseconds * 1000 );

	PrintHits(seeks, hits, seeks_length, "SequentialLookupMemory.txt");
}

void BinaryLookupMemory()
{
	SYSTEMTIME  beg;      // Start time
	SYSTEMTIME  end;      // End time

	const string keys_filepath = "key.pc.db";
	const string seeks_filepath = "seek.pc.db";

	int keys_length = 0, seeks_length;
	int* keys = NULL;
	int* seeks = NULL;
	LoadArrayFromFile(seeks_filepath, &seeks, &seeks_length);

	GetLocalTime( &beg );

	printf( "%02d:%02d:%02d:%06d\n", beg.wHour,
		beg.wMinute, beg.wSecond, beg.wMilliseconds * 1000 );

	LoadArrayFromFile(keys_filepath, &keys, &keys_length);

	int* hits = GetLookupHits_Binary(keys, keys_length, seeks, seeks_length);

	GetLocalTime( &end );
	printf( "%02d:%02d:%02d:%06d\n", end.wHour,
		end.wMinute, end.wSecond, end.wMilliseconds * 1000 );

	PrintHits(seeks, hits, seeks_length, "BinaryLookupMemory.txt");
}
