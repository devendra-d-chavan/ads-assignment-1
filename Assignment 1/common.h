#include <iostream>

using std::string;
using std::ifstream;

int LoadArrayFromFile(const string filepath, int** numbers, int* length);
int* GetLookupHits_Sequential(const int* keys, const int keys_length, const int* seeks, const int seeks_length);
int* GetLookupHits_Sequential(const string keys_filepath, const int* seeks, const int seeks_length);
int* GetLookupHits_Binary(const int* keys, const int keys_length, const int* seeks, const int seeks_length);
int* GetLookupHits_Binary(const string keys_filepath, const int* seeks, const int seeks_length);
int BinarySearch(const int* keys, const int key, const int length);
int BinarySearch(const int key, const int length, ifstream* fp);
void PrintHits(const int* seeks, const int* hits, const int seeks_length, const string output_filepath);

