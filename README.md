CSC 541: Assignment #1 - In-Memory vs. Disk-Based Searching
===========================================================
The goals of this assignment are two-fold:

To introduce you to random-access file I/O using C or C++.
To investigate time efficiency issues associated with in-memory versus 
disk-based searching strategies.
This assignment uses two "lists" of integer values: a key list and a seek list. 
The key list is a collection of integers K=k1, ..., kn representing n keys for a 
hypothetical database. The seek list is a separate collection of integers S=s1, 
..., sm representing a sequence of m requests for keys to be retrieved from the 
database.

You will implement two different search strategies to try to locate each s[i] 
from the seek list:

linear search: a sequential search of k1, k2, ..., kn for a key that matches the 
current seek value si
binary search: a search through a sorted list of keys k1, k2, ..., kn, the fact 
that the keys are sorted allows approximately half the remaining keys to be 
ignored from consideration during each step of the search
Each of the two searches (linear and binary) will be performed in two different 
environments. In the first, the key list will be held completely in memory. 
In the second, the key list will be held completely on disk.

Execution environment
--------------------------
Visual Studio Ultimate on Windows 7 64 bit